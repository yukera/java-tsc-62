package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.User;
import com.tsc.jarinchekhina.tm.repository.SessionRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import com.tsc.jarinchekhina.tm.util.SignatureUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @NotNull
    @Autowired
    private final IUserService userService;

    @NotNull
    @Autowired
    public SessionRepository sessionRepository;

    @Override
    @SneakyThrows
    public void update(@NotNull final SessionDTO session) {
        sessionRepository.save(session);
    }

    @Override
    @SneakyThrows
    public void add(@NotNull final SessionDTO session) {
        sessionRepository.save(session);
    }

    @Override
    public void addAll(@NotNull final Collection<SessionDTO> collection) {
        sessionRepository.saveAll(collection);
    }

    @Override
    public void clear() {
        sessionRepository.deleteAll();
    }

    @Override
    public void closeAll(@NotNull final List<SessionDTO> sessionList) {
        for (@Nullable final SessionDTO session : sessionList) {
            try {
                close(session);
            } catch (Exception e) {
                continue;
            }
        }
    }

    @Override
    public void close(@Nullable final SessionDTO session) {
        validate(session);
        removeById(session.getId());
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (DataUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (DataUtil.isEmpty(session.getId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @Nullable final SessionDTO tempSession = session.clone();
        if (tempSession == null) throw new AccessDeniedException();
        @Nullable final String signatureSource = session.getSignature();
        if (DataUtil.isEmpty(signatureSource)) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sign(tempSession).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        sessionRepository.findById(session.getId());
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO sign(@Nullable final SessionDTO session) {
        if (session == null) return null;
        session.setSignature(null);
        @NotNull final String secret = propertyService.getSessionSecret();
        @NotNull final Integer iteration = propertyService.getSessionIteration();
        @Nullable final String signature = SignatureUtil.sign(session, secret, iteration);
        session.setSignature(signature);
        return session;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        sessionRepository.deleteById(id);
    }

    @Override
    public @NotNull List<SessionDTO> findAll() {
        return sessionRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO findById(@NotNull String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final var session = sessionRepository.findById(id);
        if (!session.isPresent()) throw new AccessDeniedException();
        return session.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<SessionDTO> getListSession(@Nullable final SessionDTO session) {
        validate(session);
        return sessionRepository.findAllByUserId(session.getUserId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public User getUser(@Nullable final SessionDTO session) {
        @NotNull final String userId = getUserId(session);
        @Nullable final User user = userService.findById(userId);
        return user;
    }

    @NotNull
    @Override
    public String getUserId(@Nullable final SessionDTO session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO open(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (!checkDataAccess(login, password)) throw new AccessDeniedException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (user.isLocked()) throw new AccessDeniedException();
        @Nullable SessionDTO session = new SessionDTO();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        session = sign(session);
        if (session == null) throw new AccessDeniedException();
        sessionRepository.save(session);
        return session;
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) return false;
        if (DataUtil.isEmpty(password)) return false;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @Nullable String passwordHash = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(passwordHash)) return false;
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    @Transactional
    public void remove(@NotNull final SessionDTO session) {
        sessionRepository.delete(session);
    }

    @Override
    @SneakyThrows
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        @Nullable final User user = userService.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

}
