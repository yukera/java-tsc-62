package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface UserRepository extends AbstractRepository<User> {

    User save(@NotNull final User user);

    void deleteAll();

    @NotNull
    List<User> findAll();

    Optional<User> findFirstByEmail(@NotNull final String email);

    @NotNull
    Optional<User> findFirstById(@NotNull final String id);

    void delete(@NotNull final User user);

    void deleteById(@NotNull final String id);

    void deleteByLogin(@NotNull final String login);

    @NotNull
    Optional<User> findFirstByLogin(@NotNull final String login);

}
