package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IPropertyService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyEmailException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyRoleException;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.HashIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.EmailRegisteredException;
import com.tsc.jarinchekhina.tm.exception.user.LoginExistsException;
import com.tsc.jarinchekhina.tm.model.User;
import com.tsc.jarinchekhina.tm.repository.UserRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import com.tsc.jarinchekhina.tm.util.HashUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@AllArgsConstructor
public class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @Override
    @SneakyThrows
    public boolean checkRoles(@Nullable final String userId, @Nullable final Role... roles) {
        if (DataUtil.isEmpty(userId)) return false;
        if (roles == null) return true;
        @NotNull final User user = findById(userId);
        @NotNull final Role role = user.getRole();
        for (@NotNull final Role item : roles) {
            if (item.equals(role)) return true;
        }
        return false;
    }

    @NotNull
    @Override
    @SneakyThrows
    public User findById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final var user = userRepository.findById(id);
        if (!user.isPresent()) throw new UserNotFoundException();
        return user.get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@NotNull final User user) {
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void addAll(@NotNull List<User> list) {
        userRepository.saveAll(list);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void update(@NotNull final User user) {
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void clear() {
        userRepository.deleteAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
        user.setPasswordHash(hashPassword);
        userRepository.save(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        final var user = userRepository.findFirstByLogin(login);
        if (!user.isPresent()) throw new UserNotFoundException();
        return user.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        if (findByEmail(email) != null) throw new EmailRegisteredException(email);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setEmail(email);
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
        user.setPasswordHash(hashPassword);
        userRepository.save(user);
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public User findByEmail(@Nullable final String email) {
        if (DataUtil.isEmpty(email)) throw new EmptyEmailException();
        final var user = userRepository.findFirstByEmail(email);
        if (!user.isPresent()) throw new UserNotFoundException();
        return user.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        if (findByLogin(login) != null) throw new LoginExistsException(login);
        @NotNull final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setRole(role);
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
        user.setPasswordHash(hashPassword);
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User lockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        userRepository.save(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@NotNull final String id) {
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        userRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void remove(@NotNull final User user) {
        userRepository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        userRepository.deleteByLogin(login);
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User setPassword(@Nullable final String userId, @Nullable final String password) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        if (DataUtil.isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final User user = findById(userId);
        @Nullable final String hashPassword = HashUtil.salt(propertyService, password);
        if (DataUtil.isEmpty(hashPassword)) throw new HashIncorrectException();
        user.setPasswordHash(hashPassword);
        userRepository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public User unlockByLogin(@Nullable final String login) {
        if (DataUtil.isEmpty(login)) throw new EmptyLoginException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        userRepository.save(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    @Transactional
    public User update(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (DataUtil.isEmpty(userId)) throw new EmptyIdException();
        @NotNull final User user = findById(userId);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        userRepository.save(user);
        return user;
    }

}
