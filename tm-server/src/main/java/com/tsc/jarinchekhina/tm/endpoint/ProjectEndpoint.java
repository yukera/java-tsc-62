package com.tsc.jarinchekhina.tm.endpoint;

import com.tsc.jarinchekhina.tm.api.endpoint.IProjectEndpoint;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
import com.tsc.jarinchekhina.tm.api.service.ISessionService;
import com.tsc.jarinchekhina.tm.api.service.IUserService;
import com.tsc.jarinchekhina.tm.dto.ProjectDTO;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.model.Project;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    private ISessionService sessionService;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    @Autowired
    private IProjectService projectService;

    @NotNull
    @Autowired
    private IProjectTaskService projectTaskService;

    @Override
    @WebMethod
    public void changeProjectStatusById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.changeProjectStatusById(session.getUserId(), id, status);
    }

    @Override
    @WebMethod
    public void changeProjectStatusByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "status", partName = "status") @Nullable final Status status
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.changeProjectStatusByName(session.getUserId(), name, status);
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProjectWithDescription(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.create(session.getUserId(), name, description);
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Project.toDTO(projectService.findAll(session.getUserId()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Project.toDTO(projectService.findById(session.getUserId(), id));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        return Project.toDTO(projectService.findByName(session.getUserId(), name));
    }

    @Override
    @WebMethod
    public void finishProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.finishProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void finishProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.finishProjectByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectTaskService.removeProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        @NotNull Project project = projectService.findByName(session.getUserId(), name);
        projectTaskService.removeProjectById(session.getUserId(), project.getId());
    }

    @Override
    @WebMethod
    public void startProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.startProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void startProjectByName(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name", partName = "name") @Nullable final String name
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.startProjectByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id", partName = "id") @Nullable final String id,
            @WebParam(name = "name", partName = "name") @Nullable final String name,
            @WebParam(name = "description", partName = "description") @Nullable final String description
    ) {
        sessionService.validate(session);
        userService.checkRoles(session.getUserId(), Role.USER);
        projectService.updateProjectById(session.getUserId(), id, name, description);
    }

}
