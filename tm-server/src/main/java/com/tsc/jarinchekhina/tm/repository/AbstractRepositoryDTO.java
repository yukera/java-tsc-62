package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.dto.AbstractEntityDTO;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Scope("prototype")
public interface AbstractRepositoryDTO<E extends AbstractEntityDTO> extends JpaRepository<E, String> {
}
