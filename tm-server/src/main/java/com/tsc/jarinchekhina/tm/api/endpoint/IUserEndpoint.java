package com.tsc.jarinchekhina.tm.api.endpoint;

import com.tsc.jarinchekhina.tm.api.IEndpoint;
import com.tsc.jarinchekhina.tm.dto.SessionDTO;
import com.tsc.jarinchekhina.tm.dto.UserDTO;
import com.tsc.jarinchekhina.tm.enumerated.Role;
import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IUserEndpoint extends IEndpoint<User> {

    @NotNull
    UserDTO createUser(@Nullable String login, @Nullable String password);

    @NotNull
    UserDTO createUserWithEmail(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO createUserWithRole(
            @Nullable SessionDTO session,
            @Nullable String login,
            @Nullable String password,
            @Nullable Role role
    );

    @NotNull
    UserDTO setPassword(@Nullable SessionDTO session, @Nullable String password);

    @NotNull
    UserDTO updateUser(
            @Nullable SessionDTO session,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

}
