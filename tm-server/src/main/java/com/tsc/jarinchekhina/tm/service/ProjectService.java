package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.model.Project;
import com.tsc.jarinchekhina.tm.repository.ProjectRepository;
import com.tsc.jarinchekhina.tm.repository.UserRepository;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.var;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@AllArgsConstructor
public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Autowired
    public ProjectRepository projectRepository;

    @NotNull
    @Autowired
    public UserRepository userRepository;

    @Override
    @Transactional
    public void add(@NotNull final Project project) {
            projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) throw new ProjectNotFoundException();
        final var user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        project.setUser(user.get());
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void addAll(@NotNull final List<Project> list) {
        projectRepository.saveAll(list);
    }

    @Override
    @Transactional
    public void update(@NotNull final Project project) {
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        @NotNull final Project project = new Project();
        project.setName(name);
        final var user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        project.setUser(user.get());
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        final var user = userRepository.findById(userId);
        if (!user.isPresent()) throw new UserNotFoundException();
        project.setUser(user.get());
        projectRepository.save(project);
    }

    @Override
    @Transactional
    public void clear() {
        projectRepository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        projectRepository.deleteAllByUser_Id(userId);
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@Nullable final String userId) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return projectRepository.findAllByUser_Id(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@NotNull final String id) {
        final var project = projectRepository.findById(id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return project.get();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final var project = projectRepository.findFirstByUser_IdAndId(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        return project.get();
    }

    @Override
    @Transactional
    public void remove(@NotNull final Project project) {
        projectRepository.delete(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final Project project) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return;
        if (project.getUser() == null || !userId.equals(project.getUser().getId())) throw new AccessDeniedException();
        projectRepository.delete(project);
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String id) {
        projectRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final var project = projectRepository.findFirstByUser_IdAndId(userId, id);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        projectRepository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final var project = projectRepository.findFirstByUser_IdAndName(userId, name);
        if (!project.isPresent()) throw new ProjectNotFoundException();
        projectRepository.delete(project.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateProjectById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final var optionalProject = projectRepository.findFirstByUser_IdAndId(userId, id);
        if (!optionalProject.isPresent()) throw new ProjectNotFoundException();
        final var project = optionalProject.get();
        project.setName(name);
        project.setDescription(description);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeProjectStatusById(userId, id, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (status == null) throw new EmptyStatusException();
        final var optionalProject = projectRepository.findFirstByUser_IdAndId(userId, id);
        if (!optionalProject.isPresent()) throw new ProjectNotFoundException();
        final var project = optionalProject.get();
        changeProjectStatus(userId, project, status);
    }

    @SneakyThrows
    @Transactional
    public void changeProjectStatus(
            @NotNull final String userId,
            @NotNull final Project project,
            @NotNull final Status status
    ) {
        project.setStatus(status);
        projectRepository.save(project);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeProjectStatusByName(userId, name, Status.IN_PROGRESS);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeProjectStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (status == null) throw new EmptyStatusException();
        @NotNull final var project = findByName(userId, name);
        changeProjectStatus(userId, project, status);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final var optionalProject = projectRepository.findFirstByUser_IdAndName(userId, name);
        if (!optionalProject.isPresent()) throw new ProjectNotFoundException();
        return optionalProject.get();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishProjectById(@Nullable final String userId, @Nullable final String id) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        changeProjectStatusById(userId, id, Status.COMPLETED);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishProjectByName(@Nullable final String userId, @Nullable final String name) {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        changeProjectStatusByName(userId, name, Status.COMPLETED);
    }

}
