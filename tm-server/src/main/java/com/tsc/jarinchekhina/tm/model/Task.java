package com.tsc.jarinchekhina.tm.model;

import com.tsc.jarinchekhina.tm.dto.TaskDTO;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.listener.EntityListener;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Cacheable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@Entity
@Cacheable
@Table(name = "tm_task")
@EntityListeners(EntityListener.class)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Task extends AbstractBusinessEntity {

    @Column
    @NotNull
    private String name;

    @Column
    @Nullable
    private String description;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    @Nullable
    @ManyToOne
    private Project project;

    @Column(name = "date_start")
    @Nullable
    private Date dateStart;

    @Column(name = "date_finish")
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date created = new Date();

    @NotNull
    public static List<TaskDTO> toDTO(@Nullable final List<Task> taskList) {
        if (DataUtil.isEmpty(taskList)) return Collections.emptyList();
        @NotNull final List<TaskDTO> result = new ArrayList<>();
        for (@Nullable final Task task : taskList) {
            if (task == null) continue;
            result.add(Task.toDTO(task));
        }
        return result;
    }

    @Nullable
    public static TaskDTO toDTO(@Nullable final Task task) {
        if (task == null) return null;
        return new TaskDTO(task);
    }

}
