package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractRepository<Project> {

    @NotNull
    Project save(@NotNull final Project project);

    void deleteAll();

    void deleteAllByUser_Id(@NotNull final String userId);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAllByUser_Id(@NotNull final String userId);

    void delete(@NotNull final Project project);

    void deleteById(@NotNull final String id);

    @NotNull
    Optional<Project> findFirstByUser_IdAndId(@NotNull final String userId, @NotNull final String id);

    @NotNull
    Optional<Project> findFirstById(@NotNull final String id);

    void deleteByUser_IdAndName(@NotNull final String userId, @NotNull final String name);

    @NotNull
    Optional<Project> findFirstByUser_IdAndName(@NotNull final String userId, @NotNull final String name);

}
