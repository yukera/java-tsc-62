//package com.tsc.jarinchekhina.tm.service;
//
//import com.tsc.jarinchekhina.tm.api.service.IProjectService;
//import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
//import com.tsc.jarinchekhina.tm.api.service.ITaskService;
//import com.tsc.jarinchekhina.tm.common.CommonTest;
//import com.tsc.jarinchekhina.tm.component.Bootstrap;
//import com.tsc.jarinchekhina.tm.enumerated.Status;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
//import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
//import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
//import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
//import com.tsc.jarinchekhina.tm.model.Project;
//import com.tsc.jarinchekhina.tm.model.Task;
//import com.tsc.jarinchekhina.tm.model.User;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.List;
//
//@SpringBootTest
//public class ProjectServiceTest extends CommonTest {
//
//    @Autowired
//    private ProjectTaskService projectTaskService;
//
//    @Autowired
//    private IProjectService projectService;
//
//    @Autowired
//    private ITaskService taskService;
//
//    @NotNull
//    private static final Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private static User user;
//
//    @Nullable
//    private static String userId;
//
//    @BeforeClass
//    public static void beforeClass() {
//        user = bootstrap.getUserService().findByLogin("auto");
//        if (user == null) user = bootstrap.getUserService().create("auto", "auto");
//        userId = user.getId();
//    }
//
//    @After
//    public void after() {
//        projectTaskService.clearProjects(userId);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindAllNoUserId() {
//        projectService.findAll("");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testAddNoUserId() {
//        projectService.add("", null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCreateNoUserId() {
//        projectService.create("", "AUTO Project");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testCreateNoName() {
//        projectService.create(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCreateWithDescriptionNoUserId() {
//        projectService.create("", "AUTO Project", "Desc");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testCreateWithDescriptionNoName() {
//        projectService.create(userId, "", "Desc");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void testCreateWithDescriptionNoDescription() {
//        projectService.create(userId, "AUTO Project", "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveNoUserId() {
//        projectService.remove("", null);
//    }
//
//    @Test
//    public void testCreateFindAllRemove() {
//        projectService.create(userId, "AUTO Project 2");
//        projectService.create(userId, "AUTO Project 1", "desc");
//        @NotNull List<Project> projectList = projectService.findAll(userId);
//        Assert.assertEquals(2, projectList.size());
//
//        projectService.remove(userId, projectList.get(0));
//        projectService.remove(userId, projectList.get(1));
//        projectList = projectService.findAll(userId);
//        Assert.assertEquals(0, projectList.size());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindByIdNoUserId() {
//        projectService.findById("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testFindByIdNoId() {
//        projectService.findById(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindByIndexNoUserId() {
//        projectService.findByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testFindByIndexNoId() {
//        projectService.findByIndex(userId, null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindByNameNoUserId() {
//        projectService.findByName("", "1");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testFindByNameNoName() {
//        projectService.findByName(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveByIdNoUserId() {
//        projectService.removeById("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testRemoveByIdNoId() {
//        projectService.removeById(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveByIndexNoUserId() {
//        projectService.removeByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testRemoveByIndexNoId() {
//        projectService.removeByIndex(userId, null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveByNameNoUserId() {
//        projectService.removeByName("", "1");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testRemoveByNameNoName() {
//        projectService.removeByName(userId, "");
//    }
//
//    @Test
//    public void testCreateFindRemove() {
//        projectService.create(userId, "AUTO Project CFR Id");
//        projectService.create(userId, "AUTO Project CFR Index");
//        projectService.create(userId, "AUTO Project CFR Name");
//
//        @NotNull final Project projectById = projectService.findByName(userId, "AUTO Project CFR Id");
//        @NotNull final Project projectByName = projectService
//                                                        .findByName(userId, "AUTO Project CFR Name");
//        @NotNull Project projectFound = projectService.findById(userId, projectById.getId());
//        Assert.assertNotNull(projectFound);
//        Assert.assertEquals("AUTO Project CFR Id", projectFound.getName());
//
//        projectFound = projectService.findByName(userId, "AUTO Project CFR Name");
//        Assert.assertNotNull(projectFound);
//        Assert.assertEquals(projectByName.getId(), projectFound.getId());
//
//        projectService.removeById(userId, projectById.getId());
//        projectService.removeByName(userId, "AUTO Project CFR Name");
//        projectService.removeByIndex(userId, 1);
//
//        @NotNull final List<Project> projectList = projectService.findAll(userId);
//        Assert.assertEquals(0, projectList.size());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testUpdateProjectByIdNoUserId() {
//        projectService.updateProjectById("", "1", "123", "456");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testUpdateProjectByIdNoId() {
//        projectService.updateProjectById(userId, "", "123", "456");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testUpdateProjectByIdNoName() {
//        projectService.updateProjectById(userId, "1", "", "456");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void testUpdateProjectByIdNoDescription() {
//        projectService.updateProjectById(userId, "1", "123", "");
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testUpdateProjectByIdNoProject() {
//        projectService.updateProjectById(userId, "1", "123", "456");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testUpdateProjectByIndexNoUserId() {
//        projectService.updateProjectByIndex("", 1, "123", "456");
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testUpdateProjectByIndexNoIndex() {
//        projectService.updateProjectByIndex(userId, null, "123", "456");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testUpdateProjectByIndexNoName() {
//        projectService.updateProjectByIndex(userId, 1, "", "456");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void testUpdateProjectByIndexNoDescription() {
//        projectService.updateProjectByIndex(userId, 1, "123", "");
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testUpdateProjectByIndexNoProject() {
//        projectService.updateProjectByIndex(userId, 1, "123", "456");
//    }
//
//    @Test
//    public void testUpdateProject() {
//        projectService.create(userId, "AUTO", "AUTO");
//        projectService.create(userId, "AUTO 2", "AUTO 2");
//        @NotNull Project projectById = projectService.findByName(userId, "AUTO");
//
//        projectService.updateProjectById(userId, projectById.getId(), "AUTO1", "AUTO1D");
//        projectById = projectService.findByName(userId, "AUTO1");
//        Assert.assertEquals("AUTO1D", projectById.getDescription());
//
//        projectService.updateProjectByIndex(userId, 1, "AUTO2", "AUTO2D");
//        @NotNull final Project projectByName = projectService.findByName(userId, "AUTO2");
//        Assert.assertEquals("AUTO2D", projectByName.getDescription());
//
//        projectService.removeById(userId, projectById.getId());
//        projectService.removeByIndex(userId, 1);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testStartProjectByIdNoUserId() {
//        projectService.startProjectById("", "");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testStartProjectByIdNoId() {
//        projectService.startProjectById(userId, "");
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testStartProjectByIdNoProject() {
//        projectService.startProjectById(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testStartProjectByIndexNoUserId() {
//        projectService.startProjectByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testStartProjectByIndexNoIndex() {
//        projectService.startProjectByIndex(userId, null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testStartProjectByIndexNoProject() {
//        projectService.startProjectByIndex(userId, 1);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testStartProjectByNameNoUserId() {
//        projectService.startProjectByName("", "123");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testStartProjectByNameNoName() {
//        projectService.startProjectByName(userId, null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testStartProjectByNameNoProject() {
//        projectService.startProjectByName(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFinishProjectByIdNoUserId() {
//        projectService.finishProjectById("", "");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testFinishProjectByIdNoId() {
//        projectService.finishProjectById(userId, "");
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testFinishProjectByIdNoProject() {
//        projectService.finishProjectById(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFinishProjectByIndexNoUserId() {
//        projectService.finishProjectByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testFinishProjectByIndexNoIndex() {
//        projectService.finishProjectByIndex(userId, null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testFinishProjectByIndexNoProject() {
//        projectService.finishProjectByIndex(userId, 1);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFinishProjectByNameNoUserId() {
//        projectService.finishProjectByName("", "123");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testFinishProjectByNameNoName() {
//        projectService.finishProjectByName(userId, null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testFinishProjectByNameNoProject() {
//        projectService.finishProjectByName(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testChangeProjectStatusByIdNoUserId() {
//        projectService.changeProjectStatusById("", "", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testChangeProjectStatusByIdNoId() {
//        projectService.changeProjectStatusById(userId, "", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyStatusException.class)
//    public void testChangeProjectStatusByIdNoStatus() {
//        projectService.changeProjectStatusById(userId, "1", null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testChangeProjectStatusByIdNoProject() {
//        projectService.changeProjectStatusById(userId, "1", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testChangeProjectStatusByIndexNoUserId() {
//        projectService.changeProjectStatusByIndex("", 1, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testChangeProjectStatusByIndexNoIndex() {
//        projectService.changeProjectStatusByIndex(userId, null, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyStatusException.class)
//    public void testChangeProjectStatusByIndexNoStatus() {
//        projectService.changeProjectStatusByIndex(userId, 1, null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testChangeProjectStatusByIndexNoProject() {
//        projectService.changeProjectStatusByIndex(userId, 1, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testChangeProjectStatusByNameNoUserId() {
//        projectService.changeProjectStatusByName("", "123", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testChangeProjectStatusByNameNoName() {
//        projectService.changeProjectStatusByName(userId, null, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyStatusException.class)
//    public void testChangeProjectStatusByNameNoStatus() {
//        projectService.changeProjectStatusByName(userId, "1", null);
//    }
//
//    @Test(expected = ProjectNotFoundException.class)
//    public void testChangeProjectStatusByNameNoProject() {
//        projectService.changeProjectStatusByName(userId, "1", Status.IN_PROGRESS);
//    }
//
//    @Test
//    public void testStatusField() {
//        projectService.create(userId, "AUTO Project Id");
//        @NotNull final Project projectById = projectService.findByName(userId, "AUTO Project Id");
//        Assert.assertEquals(Status.NOT_STARTED, projectById.getStatus());
//        projectService.create(userId, "AUTO Project Name");
//        @NotNull final Project projectByName = projectService.findByName(userId, "AUTO Project Name");
//        Assert.assertEquals(Status.NOT_STARTED, projectByName.getStatus());
//
//        projectService.startProjectById(userId, projectById.getId());
//        @NotNull Project project = projectService.findByName(userId, "AUTO Project Id");
//        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
//        projectService.startProjectByName(userId, "AUTO Project Name");
//        project = projectService.findByName(userId, "AUTO Project Name");
//        Assert.assertEquals(Status.IN_PROGRESS, project.getStatus());
//
//        projectService.finishProjectById(userId, projectById.getId());
//        project = projectService.findByName(userId, "AUTO Project Id");
//        Assert.assertEquals(Status.COMPLETED, project.getStatus());
//        projectService.finishProjectByName(userId, "AUTO Project Name");
//        project = projectService.findByName(userId, "AUTO Project Name");
//        Assert.assertEquals(Status.COMPLETED, project.getStatus());
//
//        projectService.changeProjectStatusById(userId, projectById.getId(), Status.NOT_STARTED);
//        project = projectService.findByName(userId, "AUTO Project Id");
//        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
//        projectService.changeProjectStatusByName(userId, "AUTO Project Name", Status.NOT_STARTED);
//        project = projectService.findByName(userId, "AUTO Project Name");
//        Assert.assertEquals(Status.NOT_STARTED, project.getStatus());
//
//        projectService.remove(userId, projectById);
//        projectService.remove(userId, projectByName);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveProjectByIdNoUserId() {
//        projectTaskService.removeProjectById("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testRemoveProjectByIdNoId() {
//        projectTaskService.removeProjectById(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testClearProjectsNoUserId() {
//        projectTaskService.clearProjects("");
//    }
//
//    @Test
//    public void testRemoveClearProjects() {
//        projectService.create(userId, "AUTO Pr 1");
//        @NotNull final Project projectFirst = projectService.findByName(userId, "AUTO Pr 1");
//        projectService.create(userId, "AUTO Pr 2");
//        @NotNull final Project projectSecond = projectService.findByName(userId, "AUTO Pr 2");
//        projectService.create(userId, "AUTO Pr 3");
//        taskService.create(userId, "AUTO T 1");
//        @NotNull final Task taskFirst = taskService.findByName(userId, "AUTO T 1");
//        taskService.create(userId, "AUTO T 2");
//        @NotNull final Task taskSecond = taskService.findByName(userId, "AUTO T 2");
//        taskService.create(userId, "AUTO T 3");
//        @NotNull final Task taskThird = taskService.findByName(userId, "AUTO T 3");
//
//        projectTaskService.bindTaskByProjectId(userId, projectFirst.getId(), taskFirst.getId());
//        projectTaskService.bindTaskByProjectId(userId, projectSecond.getId(), taskSecond.getId());
//        projectTaskService.bindTaskByProjectId(userId, projectSecond.getId(), taskThird.getId());
//
//        projectTaskService.removeProjectById(userId, projectFirst.getId());
//        @NotNull Project project = projectService.findByName(userId, "AUTO Pr 2");
//        Assert.assertNotNull(project);
//        @NotNull Task task = taskService.findByName(userId, "AUTO T 2");
//        Assert.assertNotNull(task);
//
//        projectTaskService.clearProjects(userId);
//    }
//
//}
