//package com.tsc.jarinchekhina.tm.service;
//
//import com.tsc.jarinchekhina.tm.api.service.IProjectService;
//import com.tsc.jarinchekhina.tm.api.service.IProjectTaskService;
//import com.tsc.jarinchekhina.tm.api.service.ITaskService;
//import com.tsc.jarinchekhina.tm.common.CommonTest;
//import com.tsc.jarinchekhina.tm.component.Bootstrap;
//import com.tsc.jarinchekhina.tm.enumerated.Status;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyStatusException;
//import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
//import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
//import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
//import com.tsc.jarinchekhina.tm.model.Project;
//import com.tsc.jarinchekhina.tm.model.Task;
//import com.tsc.jarinchekhina.tm.model.User;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.BeforeEach;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.List;
//
//public class TaskServiceTest extends CommonTest {
//
//    @NotNull
//    private final Bootstrap bootstrap = context.getBean(Bootstrap.class);
//
//    @Nullable
//    private static User user;
//
//    @Nullable
//    private static String userId;
//
//    @Autowired
//    private ITaskService taskService;
//
//    @Autowired
//    private IProjectTaskService projectTaskService;
//
//    @Autowired
//    private IProjectService projectService;
//
//    @BeforeEach
//    public void beforeClass() {
//        user = bootstrap.getUserService().findByLogin("auto");
//        if (user == null) user = bootstrap.getUserService().create("auto", "auto");
//        userId = user.getId();
//    }
//
//    @AfterEach
//    public void afterClass() {
//        bootstrap.getUserService().removeByLogin("auto");
//    }
//
//    @After
//    public void after() {
//        taskService.clear(userId);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindAllNoUserId() {
//        taskService.findAll("");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testAddNoUserId() {
//        taskService.add("", null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCreateNoUserId() {
//        taskService.create("", "AUTO Task");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testCreateNoName() {
//        taskService.create(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCreateWithDescriptionNoUserId() {
//        taskService.create("", "AUTO Task", "Desc");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testCreateWithDescriptionNoName() {
//        taskService.create(userId, "", "Desc");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void testCreateWithDescriptionNoDescription() {
//        taskService.create(userId, "AUTO Task", "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveNoUserId() {
//        taskService.remove("", null);
//    }
//
//    @Test
//    public void testCreateFindAllRemove() {
//        @Nullable Task task = new Task();
//        task.setName("AUTO Task 3");
//        task.setUser(user);
//        taskService.add(userId, task);
//
//        taskService.create(userId, "AUTO Task 2");
//        taskService.create(userId, "AUTO Task 1", "desc");
//        @NotNull List<Task> taskList = taskService.findAll(userId);
//        Assert.assertEquals(3, taskList.size());
//
//        taskService.remove(userId, taskList.get(0));
//        taskService.remove(userId, taskList.get(1));
//        taskService.remove(userId, taskList.get(2));
//        taskList = taskService.findAll(userId);
//        Assert.assertEquals(0, taskList.size());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindByIdNoUserId() {
//        taskService.findById("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testFindByIdNoId() {
//        taskService.findById(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindByIndexNoUserId() {
//        taskService.findByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testFindByIndexNoId() {
//        taskService.findByIndex(userId, null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindByNameNoUserId() {
//        taskService.findByName("", "1");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testFindByNameNoName() {
//        taskService.findByName(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveByIdNoUserId() {
//        taskService.removeById("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testRemoveByIdNoId() {
//        taskService.removeById(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveByIndexNoUserId() {
//        taskService.removeByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testRemoveByIndexNoId() {
//        taskService.removeByIndex(userId, null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testRemoveByNameNoUserId() {
//        taskService.removeByName("", "1");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testRemoveByNameNoName() {
//        taskService.removeByName(userId, "");
//    }
//
//    @Test
//    public void testCreateFindRemove() {
//        taskService.create(userId, "AUTO Task CFR Id");
//        taskService.create(userId, "AUTO Task CFR Index");
//        taskService.create(userId, "AUTO Task CFR Name");
//
//        @NotNull final Task taskById = taskService.findByName(userId, "AUTO Task CFR Id");
//        @NotNull final Task taskByName = taskService.findByName(userId, "AUTO Task CFR Name");
//        @NotNull Task taskFound = taskService.findById(userId, taskById.getId());
//        Assert.assertNotNull(taskFound);
//        Assert.assertEquals("AUTO Task CFR Id", taskFound.getName());
//
//        taskFound = taskService.findByIndex(userId, 1);
//        @Nullable List<Task> taskList = taskService.findAll(userId);
//        Assert.assertNotNull(taskFound);
//        Assert.assertEquals(taskList.get(0).getId(), taskFound.getId());
//
//        taskFound = taskService.findByName(userId, "AUTO Task CFR Name");
//        Assert.assertNotNull(taskFound);
//        Assert.assertEquals(taskByName.getId(), taskFound.getId());
//
//        taskService.removeById(userId, taskById.getId());
//        taskService.removeByName(userId, "AUTO Task CFR Name");
//        taskService.removeByIndex(userId, 1);
//
//        taskList = taskService.findAll(userId);
//        Assert.assertEquals(0, taskList.size());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testUpdateTaskByIdNoUserId() {
//        taskService.updateTaskById("", "1", "123", "456");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testUpdateTaskByIdNoId() {
//        taskService.updateTaskById(userId, "", "123", "456");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testUpdateTaskByIdNoName() {
//        taskService.updateTaskById(userId, "1", "", "456");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void testUpdateTaskByIdNoDescription() {
//        taskService.updateTaskById(userId, "1", "123", "");
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testUpdateTaskByIdNoTask() {
//        taskService.updateTaskById(userId, "1", "123", "456");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testUpdateTaskByIndexNoUserId() {
//        taskService.updateTaskByIndex("", 1, "123", "456");
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testUpdateTaskByIndexNoIndex() {
//        taskService.updateTaskByIndex(userId, null, "123", "456");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testUpdateTaskByIndexNoName() {
//        taskService.updateTaskByIndex(userId, 1, "", "456");
//    }
//
//    @Test(expected = EmptyDescriptionException.class)
//    public void testUpdateTaskByIndexNoDescription() {
//        taskService.updateTaskByIndex(userId, 1, "123", "");
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testUpdateTaskByIndexNoTask() {
//        taskService.updateTaskByIndex(userId, 1, "123", "456");
//    }
//
//    @Test
//    public void testUpdateTask() {
//        taskService.create(userId, "AUTO", "AUTO");
//        taskService.create(userId, "AUTO 2", "AUTO 2");
//        @NotNull Task taskById = taskService.findByName(userId, "AUTO");
//
//        taskService.updateTaskById(userId, taskById.getId(), "AUTO1", "AUTO1D");
//        taskById = taskService.findByName(userId, "AUTO1");
//        Assert.assertEquals("AUTO1D", taskById.getDescription());
//
//        taskService.updateTaskByIndex(userId, 1, "AUTO2", "AUTO2D");
//        @NotNull final Task taskByName = taskService.findByName(userId, "AUTO2");
//        Assert.assertEquals("AUTO2D", taskByName.getDescription());
//
//        taskService.removeByIndex(userId, 1);
//        taskService.removeByIndex(userId, 1);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testStartTaskByIdNoUserId() {
//        taskService.startTaskById("", "");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testStartTaskByIdNoId() {
//        taskService.startTaskById(userId, "");
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testStartTaskByIdNoTask() {
//        taskService.startTaskById(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testStartTaskByIndexNoUserId() {
//        taskService.startTaskByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testStartTaskByIndexNoIndex() {
//        taskService.startTaskByIndex(userId, null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testStartTaskByIndexNoTask() {
//        taskService.startTaskByIndex(userId, 1);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testStartTaskByNameNoUserId() {
//        taskService.startTaskByName("", "123");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testStartTaskByNameNoName() {
//        taskService.startTaskByName(userId, null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testStartTaskByNameNoTask() {
//        taskService.startTaskByName(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFinishTaskByIdNoUserId() {
//        taskService.finishTaskById("", "");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testFinishTaskByIdNoId() {
//        taskService.finishTaskById(userId, "");
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testFinishTaskByIdNoTask() {
//        taskService.finishTaskById(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFinishTaskByIndexNoUserId() {
//        taskService.finishTaskByIndex("", 1);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testFinishTaskByIndexNoIndex() {
//        taskService.finishTaskByIndex(userId, null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testFinishTaskByIndexNoTask() {
//        taskService.finishTaskByIndex(userId, 1);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFinishTaskByNameNoUserId() {
//        taskService.finishTaskByName("", "123");
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testFinishTaskByNameNoName() {
//        taskService.finishTaskByName(userId, null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testFinishTaskByNameNoTask() {
//        taskService.finishTaskByName(userId, "1");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testChangeTaskStatusByIdNoUserId() {
//        taskService.changeTaskStatusById("", "", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testChangeTaskStatusByIdNoId() {
//        taskService.changeTaskStatusById(userId, "", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyStatusException.class)
//    public void testChangeTaskStatusByIdNoStatus() {
//        taskService.changeTaskStatusById(userId, "1", null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testChangeTaskStatusByIdNoTask() {
//        taskService.changeTaskStatusById(userId, "1", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testChangeTaskStatusByIndexNoUserId() {
//        taskService.changeTaskStatusByIndex("", 1, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = IndexIncorrectException.class)
//    public void testChangeTaskStatusByIndexNoIndex() {
//        taskService.changeTaskStatusByIndex(userId, null, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyStatusException.class)
//    public void testChangeTaskStatusByIndexNoStatus() {
//        taskService.changeTaskStatusByIndex(userId, 1, null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testChangeTaskStatusByIndexNoTask() {
//        taskService.changeTaskStatusByIndex(userId, 1, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testChangeTaskStatusByNameNoUserId() {
//        taskService.changeTaskStatusByName("", "123", Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyNameException.class)
//    public void testChangeTaskStatusByNameNoName() {
//        taskService.changeTaskStatusByName(userId, null, Status.IN_PROGRESS);
//    }
//
//    @Test(expected = EmptyStatusException.class)
//    public void testChangeTaskStatusByNameNoStatus() {
//        taskService.changeTaskStatusByName(userId, "1", null);
//    }
//
//    @Test(expected = TaskNotFoundException.class)
//    public void testChangeTaskStatusByNameNoTask() {
//        taskService.changeTaskStatusByName(userId, "1", Status.IN_PROGRESS);
//    }
//
//    @Test
//    public void testStatusField() {
//        taskService.create(userId, "AUTO Task Id");
//        @NotNull final Task taskById = taskService.findByName(userId, "AUTO Task Id");
//        Assert.assertEquals(Status.NOT_STARTED, taskById.getStatus());
//        taskService.create(userId, "AUTO Task Name");
//        @NotNull final Task taskByName = taskService.findByName(userId, "AUTO Task Name");
//        Assert.assertEquals(Status.NOT_STARTED, taskByName.getStatus());
//
//        taskService.startTaskById(userId, taskById.getId());
//        @NotNull Task task = taskService.findByName(userId, "AUTO Task Id");
//        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
//        taskService.startTaskByName(userId, "AUTO Task Name");
//        task = taskService.findByName(userId, "AUTO Task Name");
//        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
//
//        taskService.finishTaskById(userId, taskById.getId());
//        task = taskService.findByName(userId, "AUTO Task Id");
//        Assert.assertEquals(Status.COMPLETED, task.getStatus());
//        taskService.finishTaskByName(userId, "AUTO Task Name");
//        task = taskService.findByName(userId, "AUTO Task Name");
//        Assert.assertEquals(Status.COMPLETED, task.getStatus());
//
//        taskService.changeTaskStatusById(userId, taskById.getId(), Status.NOT_STARTED);
//        task = taskService.findByName(userId, "AUTO Task Id");
//        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
//        taskService.changeTaskStatusByName(userId, "AUTO Task Name", Status.NOT_STARTED);
//        task = taskService.findByName(userId, "AUTO Task Name");
//        Assert.assertEquals(Status.NOT_STARTED, task.getStatus());
//
//        taskService.remove(userId, taskById);
//        taskService.remove(userId, taskByName);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testFindAllByProjectIdNoUserId() {
//        projectTaskService.findAllTaskByProjectId("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testFindAllByProjectIdNoProjectId() {
//        projectTaskService.findAllTaskByProjectId(userId, "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testBindTaskNoUserId() {
//        projectTaskService.bindTaskByProjectId("", "1", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testBindTaskNoProjectId() {
//        projectTaskService.bindTaskByProjectId(userId, "", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testBindTaskNoTaskId() {
//        projectTaskService.bindTaskByProjectId(userId, "1", "");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testUnbindTaskNoUserId() {
//        projectTaskService.unbindTaskByProjectId("", "1");
//    }
//
//    @Test(expected = EmptyIdException.class)
//    public void testUnbindTaskNoTaskId() {
//        projectTaskService.unbindTaskByProjectId(userId, "");
//    }
//
//    @Test
//    public void testBindUnbindTask() {
//        projectService.create(userId, "AUTO Project");
//        @NotNull final Project project = projectService.findByName(userId, "AUTO Project");
//        taskService.create(userId, "AUTO Task 1");
//        @NotNull final Task taskFirst = taskService.findByName(userId, "AUTO Task 1");
//        taskService.create(userId, "AUTO Task 2");
//        @NotNull final Task taskSecond = taskService.findByName(userId, "AUTO Task 2");
//
//        projectTaskService.bindTaskByProjectId(userId, project.getId(), taskFirst.getId());
//        projectTaskService.bindTaskByProjectId(userId, project.getId(), taskSecond.getId());
//        @NotNull List<Task> taskList = projectTaskService
//                                                .findAllTaskByProjectId(userId, project.getId());
//        Assert.assertEquals(2, taskList.size());
//        Assert.assertEquals(project.getId(), taskList.get(0).getProject().getId());
//        Assert.assertEquals(project.getId(), taskList.get(1).getProject().getId());
//
//        projectTaskService.unbindTaskByProjectId(userId, taskFirst.getId());
//        taskList = projectTaskService.findAllTaskByProjectId(userId, project.getId());
//        Assert.assertEquals(1, taskList.size());
//
//        taskService.remove(userId, taskFirst);
//        taskService.remove(userId, taskSecond);
//        projectService.remove(userId, project);
//        taskList = taskService.findAll(userId);
//        Assert.assertEquals(0, taskList.size());
//    }
//
//}
