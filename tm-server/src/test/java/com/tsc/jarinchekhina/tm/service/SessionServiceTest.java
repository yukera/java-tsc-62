//package com.tsc.jarinchekhina.tm.service;
//
//import com.tsc.jarinchekhina.tm.api.service.ISessionService;
//import com.tsc.jarinchekhina.tm.common.CommonTest;
//import com.tsc.jarinchekhina.tm.component.Bootstrap;
//import com.tsc.jarinchekhina.tm.dto.SessionDTO;
//import com.tsc.jarinchekhina.tm.enumerated.Role;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyLoginException;
//import com.tsc.jarinchekhina.tm.exception.empty.EmptyPasswordException;
//import com.tsc.jarinchekhina.tm.exception.entity.UserNotFoundException;
//import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
//import com.tsc.jarinchekhina.tm.model.User;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//
//import java.util.List;
//
//public class SessionServiceTest extends CommonTest {
//
//    @NotNull
//    private static final Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private static User user;
//
//    @Autowired
//    private ISessionService sessionService;
//
//    @BeforeClass
//    public static void beforeClass() {
//        user = bootstrap.getUserService().findByLogin("auto");
//        if (user == null) user = bootstrap.getUserService().create("auto", "auto");
//    }
//
//    @AfterClass
//    public static void afterClass() {
//        bootstrap.getUserService().removeByLogin("auto");
//        bootstrap.getUserService().removeByLogin("auto locked");
//    }
//
//    @After
//    public void after() {
//        sessionService.clear();
//    }
//
//    @Test
//    public void testCheckDataAccess() {
//        boolean result = sessionService.checkDataAccess("", "auto");
//        Assert.assertFalse(result);
//        result = sessionService.checkDataAccess("auto", "");
//        Assert.assertFalse(result);
//        result = sessionService.checkDataAccess("auto", "autooo");
//        Assert.assertFalse(result);
//        result = sessionService.checkDataAccess("auto", "auto");
//        Assert.assertTrue(result);
//    }
//
//    @Test(expected = EmptyLoginException.class)
//    public void testOpenSessionNoLogin() {
//        sessionService.open("", "auto");
//    }
//
//    @Test(expected = EmptyPasswordException.class)
//    public void testOpenSessionNoPassword() {
//        sessionService.open("auto", "");
//    }
//
//    @Test(expected = UserNotFoundException.class)
//    public void testOpenSessionNoUser() {
//        sessionService.open("autoooo", "auto");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testOpenSessionWrongPassword() {
//        sessionService.open("auto", "autoooo");
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testOpenSessionUserLocked() {
//        bootstrap.getUserService().create("auto locked", "auto");
//        bootstrap.getUserService().lockByLogin("auto locked");
//        sessionService.open("auto locked", "auto");
//    }
//
//    @Test
//    public void testOpenSignSession() {
//        @Nullable SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        @Nullable final String signature = session.getSignature();
//        Assert.assertNotNull(signature);
//        session.setSignature(null);
//        session = sessionService.sign(session);
//        Assert.assertNotNull(session);
//        Assert.assertEquals(signature, session.getSignature());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionNull() {
//        sessionService.validate(null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionNoSignature() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        session.setSignature(null);
//        sessionService.validate(session);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionNoTimeStamp() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        session.setTimestamp(null);
//        sessionService.validate(session);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionWrongSignature() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        session.setSignature("1111");
//        sessionService.validate(session);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionNoSessionInRepo() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        sessionService.close(session);
//        sessionService.validate(session);
//    }
//
//    @Test
//    public void testValidateSession() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        sessionService.validate(session);
//    }
//
//    @Test
//    public void testIsValid() {
//        @Nullable SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        boolean result = sessionService.isValid(session);
//        Assert.assertTrue(result);
//        session.setSignature(null);
//        result = sessionService.isValid(session);
//        Assert.assertFalse(result);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionWithRoleNull() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        sessionService.validate(session, null);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testValidateSessionWithRoleIncorrect() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        sessionService.validate(session, Role.ADMIN);
//    }
//
//    @Test
//    public void testValidateSessionWithRole() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        sessionService.validate(session, Role.USER);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testGetUserNoUser() {
//        bootstrap.getUserService().create("auto 2", "auto");
//        @Nullable final SessionDTO session = sessionService.open("auto 2", "auto");
//        Assert.assertNotNull(session);
//        sessionService.close(session);
//        bootstrap.getUserService().removeByLogin("auto 2");
//        sessionService.getUser(session);
//    }
//
//    @Test
//    public void testGetUser() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        @NotNull final User userFromSession = sessionService.getUser(session);
//        Assert.assertNotNull(userFromSession);
//        Assert.assertEquals(user.getId(), userFromSession.getId());
//    }
//
//    @Test
//    public void testGetUserId() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        @NotNull final String userId = sessionService.getUserId(session);
//        Assert.assertEquals(user.getId(), userId);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testGetListSessionValidationFail() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        session.setSignature("1111");
//        sessionService.getListSession(session);
//    }
//
//    @Test
//    public void testGetListSession() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        @Nullable final SessionDTO sessionSecond = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        Assert.assertNotNull(sessionSecond);
//        @NotNull final List<SessionDTO> sessionList = sessionService.getListSession(session);
//        Assert.assertEquals(2, sessionList.size());
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCloseSessionValidationFail() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        session.setSignature("1111");
//        sessionService.close(session);
//    }
//
//    @Test
//    public void testCloseSession() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        boolean result = sessionService.isValid(session);
//        Assert.assertTrue(result);
//        sessionService.close(session);
//        result = sessionService.isValid(session);
//        Assert.assertFalse(result);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCloseSessionClosed() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        sessionService.close(session);
//        sessionService.close(session);
//    }
//
//    @Test(expected = AccessDeniedException.class)
//    public void testCloseAllSessions() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        @Nullable SessionDTO sessionSecond = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        Assert.assertNotNull(sessionSecond);
//        @NotNull List<SessionDTO> sessionList = sessionService.getListSession(session);
//        Assert.assertEquals(2, sessionList.size());
//        @Nullable final SessionDTO sessionThird = sessionService.open("auto", "auto");
//        Assert.assertNotNull(sessionThird);
//
//        sessionService.closeAll(sessionList);
//        sessionService.validate(session);
//    }
//
//    @Test
//    public void testCloseAllSessionsSomeClosed() {
//        @Nullable final SessionDTO session = sessionService.open("auto", "auto");
//        @Nullable final SessionDTO sessionSecond = sessionService.open("auto", "auto");
//        @Nullable final SessionDTO sessionThird = sessionService.open("auto", "auto");
//        Assert.assertNotNull(session);
//        Assert.assertNotNull(sessionSecond);
//        Assert.assertNotNull(sessionThird);
//        @NotNull final List<SessionDTO> sessionList = sessionService.getListSession(session);
//        Assert.assertEquals(3, sessionList.size());
//
//        sessionService.close(sessionSecond);
//        sessionService.closeAll(sessionList);
//    }
//
//}
