//package com.tsc.jarinchekhina.tm.endpoint;
//
//import com.tsc.jarinchekhina.tm.component.Bootstrap;
//import com.tsc.jarinchekhina.tm.marker.IntegrationCategory;
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.AfterClass;
//import org.junit.Assert;
//import org.junit.BeforeClass;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//
//public class UserEndpointTest {
//
//    @NotNull
//    private static final Bootstrap bootstrap = new Bootstrap();
//
//    @Nullable
//    private static SessionDTO adminSession;
//
//    @BeforeClass
//    public static void before() {
//        adminSession = bootstrap.getSessionEndpoint().openSession("admin", "admin");
//    }
//
//    @AfterClass
//    public static void after() {
//        bootstrap.getSessionEndpoint().closeSession(adminSession);
//    }
//
//    @Test(expected = Exception.class)
//    @Category(IntegrationCategory.class)
//    public void testCreateUserExists() {
//        bootstrap.getUserEndpoint().createUser("test", "autotest");
//    }
//
//    @Test(expected = Exception.class)
//    @Category(IntegrationCategory.class)
//    public void testFindNoUser() {
//        bootstrap.getAdminUserEndpoint().findByLogin(adminSession, "autotest");
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testCreateUserWithEmail() {
//        @Nullable final UserDTO user = bootstrap.getUserEndpoint()
//                                                .createUserWithEmail("autotest", "autotest", "autotest@test.ru");
//        Assert.assertNotNull(user);
//
//        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testCreateUserWithRole() {
//        @Nullable final UserDTO user = bootstrap.getUserEndpoint()
//                                                .createUserWithRole(adminSession, "autotest", "autotest", Role.USER);
//        Assert.assertNotNull(user);
//
//        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testSetPassword() {
//        bootstrap.getUserEndpoint().createUser("autotest", "test");
//        @Nullable SessionDTO session = bootstrap.getSessionEndpoint().openSession("autotest", "test");
//        Assert.assertNotNull(session);
//        bootstrap.getUserEndpoint().setPassword(session, "autotest");
//        bootstrap.getSessionEndpoint().closeSession(session);
//
//        session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
//        Assert.assertNotNull(session);
//        bootstrap.getSessionEndpoint().closeSession(session);
//
//        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
//    }
//
//    @Test
//    @Category(IntegrationCategory.class)
//    public void testUpdateUser() {
//        bootstrap.getUserEndpoint().createUser("autotest", "autotest");
//        @Nullable SessionDTO session = bootstrap.getSessionEndpoint().openSession("autotest", "autotest");
//        Assert.assertNotNull(session);
//        @Nullable UserDTO user = bootstrap.getUserEndpoint().updateUser(session, "John", "Dow", "Alexander");
//        Assert.assertNotNull(user);
//        Assert.assertNotNull(user.getFirstName());
//        Assert.assertEquals("John", user.getFirstName());
//        Assert.assertNotNull(user.getLastName());
//        Assert.assertEquals("Dow", user.getLastName());
//        Assert.assertNotNull(user.getMiddleName());
//        Assert.assertEquals("Alexander", user.getMiddleName());
//        Assert.assertNull(user.getEmail());
//        bootstrap.getSessionEndpoint().closeSession(session);
//
//        bootstrap.getAdminUserEndpoint().removeByLogin(adminSession, "autotest");
//    }
//
//}
