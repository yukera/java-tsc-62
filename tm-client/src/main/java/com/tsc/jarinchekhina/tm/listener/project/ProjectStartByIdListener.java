package com.tsc.jarinchekhina.tm.listener.project;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractProjectListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ProjectStartByIdListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-start-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "start project by id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectStartByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[START PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getProjectEndpoint().startProjectById(serviceLocator.getSession(), id);
    }

}
