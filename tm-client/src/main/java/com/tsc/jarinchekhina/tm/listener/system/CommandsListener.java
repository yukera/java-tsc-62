package com.tsc.jarinchekhina.tm.listener.system;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public final class CommandsListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-сmd";
    }

    @NotNull
    @Override
    public String name() {
        return "system-commands";
    }

    @NotNull
    @Override
    public String description() {
        return "display list of commands";
    }

    @Override
    @EventListener(condition = "@commandsListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[COMMANDS]");
        @NotNull final Collection<String> commands = serviceLocator.getCommandService().getCommandNames();
        for (@NotNull final String command : commands) System.out.println(command);
    }

}
