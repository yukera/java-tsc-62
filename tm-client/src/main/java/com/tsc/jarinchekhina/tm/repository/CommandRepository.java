package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import com.tsc.jarinchekhina.tm.comparator.ComparatorByNameMethod;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@NoArgsConstructor
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractListener> commandList = new ArrayList<>();

    @NotNull
    private final Map<String, AbstractListener> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractListener> commands = new LinkedHashMap<>();

    @NotNull
    @Override
    public List<AbstractListener> getCommandList() {
        return commandList.stream()
                          .sorted(ComparatorByNameMethod.getInstance())
                          .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getCommands() {
        return commands.values();
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractListener command : commands.values()) {
            @Nullable final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        for (@NotNull final AbstractListener command : arguments.values()) {
            @Nullable final String name = command.name();
            if (DataUtil.isEmpty(name)) continue;
            result.add(name);
        }
        return result;
    }

    @Nullable
    @Override
    public AbstractListener getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public AbstractListener getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Override
    public void add(@NotNull final AbstractListener command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (!DataUtil.isEmpty(arg)) arguments.put(arg, command);
        if (!DataUtil.isEmpty(name)) commands.put(name, command);
    }

}
