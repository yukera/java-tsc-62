package com.tsc.jarinchekhina.tm.listener.project;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractProjectListener;
import com.tsc.jarinchekhina.tm.endpoint.Status;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public final class ProjectStatusChangeByIdListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-status-change-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "change project status by id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@projectStatusChangeByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.fromValue(statusId);
        getProjectEndpoint().changeProjectStatusById(serviceLocator.getSession(), id, status);
    }

}
