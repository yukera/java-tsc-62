package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskUnbindByProjectIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-unbind-by-project-id";
    }

    @NotNull
    @Override
    public String description() {
        return "unbind task from project by id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskUnbindByProjectIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[UNBIND TASK FROM PROJECT BY ID]");
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getTaskEndpoint().unbindTaskByProjectId(serviceLocator.getSession(), taskId);
    }

}