package com.tsc.jarinchekhina.tm.listener.user;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractUserListener;
import com.tsc.jarinchekhina.tm.endpoint.SessionEndpoint;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class UserLogoutListener extends AbstractUserListener {

    @NotNull
    @Autowired
    private SessionEndpoint sessionEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "logout from system";
    }

    @Override
    @EventListener(condition = "@userLogoutListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[LOGOUT]");
        sessionEndpoint.closeSession(serviceLocator.getSession());
        serviceLocator.setSession(null);
    }

}
