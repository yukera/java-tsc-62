package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class TaskStartByIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-start-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "start task by id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskStartByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[START TASK]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        getTaskEndpoint().startTaskById(serviceLocator.getSession(), id);
    }

}
