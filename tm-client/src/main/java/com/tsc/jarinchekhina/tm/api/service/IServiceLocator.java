package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.endpoint.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    SessionDTO getSession();

    void setSession(@Nullable SessionDTO session);

}
