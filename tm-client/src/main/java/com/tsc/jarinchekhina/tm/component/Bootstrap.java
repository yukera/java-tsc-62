package com.tsc.jarinchekhina.tm.component;

import com.tsc.jarinchekhina.tm.api.repository.ICommandRepository;
import com.tsc.jarinchekhina.tm.api.service.ICommandService;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import com.tsc.jarinchekhina.tm.endpoint.SessionDTO;
import com.tsc.jarinchekhina.tm.exception.system.UnknownCommandException;
import com.tsc.jarinchekhina.tm.repository.CommandRepository;
import com.tsc.jarinchekhina.tm.service.CommandService;
import com.tsc.jarinchekhina.tm.service.PropertyService;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Getter
@Setter
@Component
@RequiredArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @Autowired
    private final PropertyService propertyService;

    @NotNull
    @Autowired
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    @Autowired
    private final ICommandService commandService = new CommandService(commandRepository);

    @Nullable
    @Autowired
    private AbstractListener[] abstractCommands;

    @Autowired
    private ApplicationEventPublisher publisher;

    @Nullable
    private SessionDTO session = null;

    @SneakyThrows
    public void init() {
        initCommands(abstractCommands);
        start();
    }

    @SneakyThrows
    private void initCommands(@Nullable final AbstractListener[] commandList) {
        if (commandList == null) throw new UnknownCommandException("(all)");
        for (@NotNull AbstractListener command : commandList) initCommand(command);
    }

    private void initCommand(@NotNull final AbstractListener command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void start() {
        System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
        System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");
        System.out.println("*** Welcome to Task Manager ***");
        System.out.println("v" + propertyService.getAppVersion());
        while (true) {
            try {
                @NotNull final String command = TerminalUtil.nextLine();
                if (StringUtils.isEmpty(command)) return;
                publisher.publishEvent(new ConsoleEvent(command));
                System.out.println("[OK]");
            } catch (Exception e) {
                System.err.println("[FAIL]");
            }
        }
    }

}