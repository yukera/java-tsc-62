package com.tsc.jarinchekhina.tm.listener.task;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractTaskListener;
import com.tsc.jarinchekhina.tm.endpoint.Status;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Component
public final class TaskStatusChangeByIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-status-change-by-id";
    }

    @NotNull
    @Override
    public String description() {
        return "change task status by id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskStatusChangeByIdListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusId = TerminalUtil.nextLine();
        @NotNull final Status status = Status.fromValue(statusId);
        getTaskEndpoint().changeTaskStatusById(serviceLocator.getSession(), id, status);
    }

}
