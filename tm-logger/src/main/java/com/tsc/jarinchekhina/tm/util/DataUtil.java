package com.tsc.jarinchekhina.tm.util;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.Nullable;

@UtilityClass
public class DataUtil {

    public static boolean isEmpty(@Nullable final String value) {
        return (value == null || value.isEmpty());
    }

}
